package com.zhxin.threadLab.summary.chapter1;

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadInfo;
import java.lang.management.ThreadMXBean;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.Semaphore;

/**
 * @ClassName MultiThread
 * @Description //MultiThread 看看都有哪些线程
 * @Author singleZhang
 * @Email 405780096@qq.com
 * @Date 2020/12/1 0001 上午 11:10
 **/
public class MultiThread {

    public static void main(String[] args){
        //获取JAVA线程管理 MXBean
       ThreadMXBean mxBean =  ManagementFactory.getThreadMXBean();

       //获取所有线程信息
       ThreadInfo[] infos = mxBean.dumpAllThreads(false,false);

       //打印 输出main线程和多个其他线程
        for(ThreadInfo info:infos){
            System.out.println("ID:["+info.getThreadId()+"],"+info.getThreadName());
        }
        long t = System.currentTimeMillis();

        System.out.println(t);
        Date dt = new Date(t);
        System.out.println(dt);
        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:MM:SS");
        System.out.println(fmt.format(dt));


    }

    /*
     * 将时间转换为时间戳
     */
    public static String dateToStamp(String s) throws ParseException {
        String res;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = simpleDateFormat.parse(s);
        long ts = date.getTime();
        res = String.valueOf(ts);
        return res;
    }

    /*
     * 将时间戳转换为时间
     */
    public static String stampToDate(String s){
        String res;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        long lt = new Long(s);
        Date date = new Date(lt);
        res = simpleDateFormat.format(date);
        return res;
    }

}


