package com.zhxin.threadLab.lock.chapter2;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @ClassName MyService
 * @Description //ReentrantLock 实现同步2
 * @Author singleZhang
 * @Email 405780096@qq.com
 * @Date 2020/11/28 0028 下午 4:26
 **/
public class MyService {
    private Lock lock = new ReentrantLock();

    public void testMethodA(){
        try{
            lock.lock(); //加锁
            System.out.println("methodA  begin! threadName ="+ Thread.currentThread().getName()
                    +", time="+System.currentTimeMillis());
            Thread.sleep(5000);
            System.out.println("methodA  end! threadName ="+ Thread.currentThread().getName()
                    +", time="+System.currentTimeMillis());
        }catch (InterruptedException e){
            e.printStackTrace();
        }finally {
            lock.unlock(); //释放锁
        }
    }
    public void testMethodB(){
        try{
            lock.lock(); //加锁
            System.out.println("methodB  begin! threadName ="+ Thread.currentThread().getName()
                    +", time="+System.currentTimeMillis());
            Thread.sleep(5000);
            System.out.println("methodB  end! threadName ="+ Thread.currentThread().getName()
                    +", time="+System.currentTimeMillis());
        }catch (InterruptedException e){
            e.printStackTrace();
        }finally {
            lock.unlock(); //释放锁
        }
    }
}
