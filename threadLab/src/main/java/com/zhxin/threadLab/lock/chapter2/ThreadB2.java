package com.zhxin.threadLab.lock.chapter2;

/**
 * @ClassName ThreadB
 * @Description //ThreadB2
 * @Author singleZhang
 * @Email 405780096@qq.com
 * @Date 2020/11/28 0028 下午 4:31
 **/
public class ThreadB2 extends Thread {
    private MyService service;

    public ThreadB2(MyService service){
        super();
        this.service = service;
    }

    @Override
    public void run(){
        service.testMethodB();
    }
}
