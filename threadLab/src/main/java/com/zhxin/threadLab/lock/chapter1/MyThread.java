package com.zhxin.threadLab.lock.chapter1;

/**
 * @ClassName MyThread
 * @Description // MyThread
 * @Author singleZhang
 * @Email 405780096@qq.com
 * @Date 2020/11/28 0028 下午 4:15
 **/
public class MyThread extends Thread {
    private MyService service;

    public MyThread(MyService service){
        super();
        this.service = service;
    }

    @Override
    public void run(){
        service.testMethod();
    }
}
