package com.zhxin.threadLab.lock.chapter2;

/**
 * @ClassName ThreadA
 * @Description //ThreadA2
 * @Author singleZhang
 * @Email 405780096@qq.com
 * @Date 2020/11/28 0028 下午 4:30
 **/
public class ThreadA2 extends Thread {
    private MyService service;

    public ThreadA2(MyService service){
        super();
        this.service = service;
    }

    @Override
    public void run(){
        service.testMethodA();
    }
}
