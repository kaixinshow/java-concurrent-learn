package com.zhxin.threadLab.lock.chapter3;

/**
 * @ClassName RunThread
 * @Description // 正确使用Condition 实现等待、通知 示例
 * @Author singleZhang
 * @Email 405780096@qq.com
 * @Date 2020/11/28 0028 下午 4:54
 **/
public class RunThread {

    public static void main(String[] args) throws InterruptedException {
        MyService service = new MyService();
        ThreadA a = new ThreadA(service);
        a.start();
        Thread.sleep(5000);
        service.testSignal(); //成功实现等待/通知模式
    }

}
