package com.zhxin.threadLab.lock.chapter2;

/**
 * @ClassName ThreadA
 * @Description //ThreadA
 * @Author singleZhang
 * @Email 405780096@qq.com
 * @Date 2020/11/28 0028 下午 4:30
 **/
public class ThreadA extends Thread {
    private MyService service;

    public ThreadA(MyService service){
        super();
        this.service = service;
    }

    @Override
    public void run(){
        service.testMethodA();
    }
}
