package com.zhxin.threadLab.lock.chapter3;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @ClassName MyService
 * @Description //正确使用Condition 实现等待、通知
 * @Author singleZhang
 * @Email 405780096@qq.com
 * @Date 2020/11/28 0028 下午 4:47
 **/
public class MyService {
    private Lock lock = new ReentrantLock();
    public Condition condition =  lock.newCondition();
    //等待
    public void testAwait(){
        try{
            lock.lock();//需要获得同步锁
            System.out.println("await 时间为:"+System.currentTimeMillis());
            condition.await();
        }catch (InterruptedException e){
            e.printStackTrace();
        }finally {
            lock.unlock(); //释放锁
        }
    }

    //通知
    public void testSignal(){
        try{
            lock.lock();//需要获得同步锁
            System.out.println("signal 时间为:"+System.currentTimeMillis());
            condition.signal();
        }finally {
            lock.unlock(); //释放锁
        }
    }
}
