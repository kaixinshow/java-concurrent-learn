package com.zhxin.threadLab.lock.chapter2;

/**
 * @ClassName RunThread
 * @Description //ReentrantLock 实现同步2
 *
 * @Author singleZhang
 * @Email 405780096@qq.com
 * @Date 2020/11/28 0028 下午 4:33
 **/
public class RunThread {

    public static void main(String[] args) throws InterruptedException {
        /*
        * 调用lock.lock()的线程 获得了"对象监视器"，其他线程只有等待锁被释放再争抢lock
        *
        * 效果和synchronized关键字一样，线程执行的顺序是有序的。
        * */
        MyService service = new MyService();
        ThreadA a = new ThreadA(service);
        a.setName("A");
        a.start();

        ThreadA2 a2 = new ThreadA2(service);
        a2.setName("A2");
        a2.start();
        Thread.sleep(100);
        ThreadB b = new ThreadB(service);
        b.setName("B");
        b.start();
        ThreadB2 b2 = new ThreadB2(service);
        b2.setName("B2");
        b2.start();

    }
}
