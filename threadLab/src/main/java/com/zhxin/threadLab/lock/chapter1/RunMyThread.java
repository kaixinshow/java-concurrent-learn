package com.zhxin.threadLab.lock.chapter1;

/**
 * @ClassName RunMyThread
 * @Description // ReentrantLock锁测试示例
 * @Author singleZhang
 * @Email 405780096@qq.com
 * @Date 2020/11/28 0028 下午 4:16
 **/
public class RunMyThread {

    public static void main(String[] args){
        MyService s = new MyService();
        /*MyThread[] threadArray = new MyThread[5];
        for(int a=0;a<5;a++){
            threadArray[a] = new MyThread(s);
            threadArray[a].start();
        }*/

        MyThread a = new MyThread(s);
        MyThread b = new MyThread(s);
        MyThread c = new MyThread(s);
        MyThread d = new MyThread(s);
        MyThread e = new MyThread(s);
        a.start();
        b.start();
        c.start();
        d.start();
        e.start();
    }

}
