package com.zhxin.threadLab.lock.chapter1;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @ClassName MyService
 * @Description // ReentrantLock 示例
 * @Author singleZhang
 * @Email 405780096@qq.com
 * @Date 2020/11/28 0028 下午 4:04
 **/
public class MyService {
    private Lock lock = new ReentrantLock();

    public void testMethod(){
        lock.lock(); //加锁
        for(int i = 0;i<5;i++){
            System.out.println("ThreadName="+Thread.currentThread().getName()+",i="+(i+1));
        }
        lock.unlock(); //释放锁
    }
}
