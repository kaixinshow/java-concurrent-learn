package com.zhxin.threadLab.synchronize.chapter6;

/**
 * @ClassName ThreadB
 * @Description //ThreadB
 * @Author singleZhang
 * @Email 405780096@qq.com
 * @Date 2020/11/27 0027 下午 4:32
 **/
public class ThreadB extends Thread {
    private StringObject object;
    public ThreadB(StringObject object){
        super();
        this.object = object;
    }

    @Override
    public void run(){
        object.printStr("AA");
        //object.printStr(new String("AA"));
    }
}
