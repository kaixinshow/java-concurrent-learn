package com.zhxin.threadLab.synchronize.chapter2;

/**
 * @ClassName ThreadB
 * @Description //ThreadB
 * @Author singleZhang
 * @Email 405780096@qq.com
 * @Date 2020/11/27 0027 上午 10:17
 **/
public class ThreadB extends Thread {
    private MyObject object;
    public ThreadB(MyObject object){
        super();
        this.object = object;
    }

    @Override
    public void run(){
        super.run();
        object.methodA();
    }
}
