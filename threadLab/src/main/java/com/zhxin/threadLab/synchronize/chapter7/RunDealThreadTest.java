package com.zhxin.threadLab.synchronize.chapter7;

/**
 * @ClassName RunDealThreadTest
 * @Description //RunDealThreadTest 可以用jps监测是否出现死锁
 * @Author singleZhang
 * @Email 405780096@qq.com
 * @Date 2020/11/27 0027 下午 4:54
 **/
public class RunDealThreadTest {

    public static void main(String[] args){
       try{
           DealThread t1 = new DealThread();
           t1.setFlag("zhangsan");
           Thread thread1 = new Thread(t1);
           thread1.start();
           Thread.sleep(100);
           t1.setFlag("lisi123");
           Thread thread2 = new Thread(t1);
           thread2.start();
       }catch (InterruptedException e){
           e.printStackTrace();
       }

    }

}
