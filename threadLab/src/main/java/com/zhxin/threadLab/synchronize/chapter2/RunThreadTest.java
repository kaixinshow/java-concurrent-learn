package com.zhxin.threadLab.synchronize.chapter2;

/**
 * @ClassName RunThreadTest
 * @Description // synchronized方法与锁对象
 *              // 1. MyObject methodA方法前 未加 synchronized
 *              // 2. MyObject methodA方法前 加 synchronized
 * @Author singleZhang
 * @Email 405780096@qq.com
 * @Date 2020/11/27 0027 上午 10:26
 **/
public class RunThreadTest {

    public static void main(String[] args){
        /*
        1,2
        * 有synchronized关键字的方法，一定是排队运行的
        * 只有 共享资源 的读写，才需要同步
        * */
/*        MyObject object = new MyObject();
        ThreadA a = new ThreadA(object);
        a.setName("a");
        ThreadB b = new ThreadB(object);
        b.setName("b");
        a.start();
        b.start();*/

        /*
        * 下面看示例3 对MyObject改造，代码查看MyObject2
        * 3. b调用methodB 方法前 未加 synchronized
        * 4. b调用methodB 方法前 加 synchronized
        * 3说明 A线程持有object对象锁的时候，b线程可以调用object对象非synchronized方法methodB 执行
        * 4说明 A线程持有object对象锁的时候，B线程调用object其他synchronized方法methodB的时候 需要等待
        * */
        MyObject2 object = new MyObject2();
        ThreadA2 a = new ThreadA2(object);
        a.setName("a");
        ThreadB2 b = new ThreadB2(object);
        b.setName("b");
        a.start();
        b.start();
    }
}
