package com.zhxin.threadLab.synchronize.chapter1;

/**
 * @ClassName ThreadB
 * @Description //TODO
 * @Author singleZhang
 * @Email 405780096@qq.com
 * @Date 2020/11/27 0027 上午 8:59
 **/
public class ThreadB extends Thread {
    private PrivateNumber privateNumber;
    public ThreadB(PrivateNumber privateNumber){
        super();
        this.privateNumber = privateNumber;
    }

    @Override
    public void run(){
        super.run();
        privateNumber.changeNumByName("b");
    }
}
