package com.zhxin.threadLab.synchronize.chapter1;

/**
 * @ClassName PrivateNumber
 * @Description // 实例变量非线程安全/线程安全
 *              // 1. changeNumByName 前不加synchronized
 *              // 2. changeNumByName 前加synchronized
 * @Author singleZhang
 * @Email 405780096@qq.com
 * @Date 2020/11/27 0027 上午 8:52
 **/
public class PrivateNumber {

    private int num = 0;

    synchronized public void changeNumByName(String name){
        try{
            if(name.equals("a")){
                num = 100;
                System.out.println("num changed by a!");
                Thread.sleep(1000);
            } else {
                num = 200;
                System.out.println("num changed by b!");
            }
            System.out.println("name="+name+",num="+num);
        }catch (InterruptedException e){
            e.printStackTrace();
        }
    }

}
