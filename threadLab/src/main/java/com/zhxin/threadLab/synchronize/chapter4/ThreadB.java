package com.zhxin.threadLab.synchronize.chapter4;

/**
 * @ClassName ThreadB
 * @Description //ThreadB
 * @Author singleZhang
 * @Email 405780096@qq.com
 * @Date 2020/11/27 0027 上午 11:56
 **/
public class ThreadB extends Thread {
    private SynchronizedObject object;
    public ThreadB(SynchronizedObject object){
        super();
        this.object = object;
    }

    @Override
    public void run(){
        super.run();
        object.ObjectMethod();
    }
}
