package com.zhxin.threadLab.synchronize.chapter2;

/**
 * @ClassName MyObject
 * @Description // synchronized方法与锁对象
 *                // 1. MyObject methodA方法前 未加 synchronized
 *                // 2. MyObject methodA方法前 加 synchronized
 * @Author singleZhang
 * @Email 405780096@qq.com
 * @Date 2020/11/27 0027 上午 10:11
 **/
public class MyObject {


    synchronized public void methodA(){
        try{
            System.out.println("begin-----methodA,current threadName:"+Thread.currentThread().getName());
            Thread.sleep(1000);
            System.out.println("end!");
        }catch (InterruptedException e){
            e.printStackTrace();
        }
    }
}
