package com.zhxin.threadLab.synchronize.chapter6;

/**
 * @ClassName ThreadA
 * @Description //ThreadA
 * @Author singleZhang
 * @Email 405780096@qq.com
 * @Date 2020/11/27 0027 下午 4:31
 **/
public class ThreadA extends Thread {
    private StringObject object;
    public ThreadA(StringObject object){
        super();
        this.object = object;
    }

    @Override
    public void run(){
        object.printStr("AA");
        //object.printStr(new String("AA"));
    }
}
