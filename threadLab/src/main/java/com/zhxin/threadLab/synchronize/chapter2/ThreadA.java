package com.zhxin.threadLab.synchronize.chapter2;

/**
 * @ClassName ThreadA
 * @Description //ThreadA
 * @Author singleZhang
 * @Email 405780096@qq.com
 * @Date 2020/11/27 0027 上午 10:14
 **/
public class ThreadA extends Thread {
    private MyObject object;
    public ThreadA(MyObject object){
        super();
        this.object = object;
    }

    @Override
    public void run(){
        super.run();
        object.methodA();
    }
}
