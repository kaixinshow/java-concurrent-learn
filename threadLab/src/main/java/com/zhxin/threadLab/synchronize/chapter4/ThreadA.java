package com.zhxin.threadLab.synchronize.chapter4;

/**
 * @ClassName ThreadA
 * @Description // ThreadA
 * @Author singleZhang
 * @Email 405780096@qq.com
 * @Date 2020/11/27 0027 上午 11:53
 **/
public class ThreadA extends Thread {
    private SynchronizedObject object;
    public ThreadA(SynchronizedObject object){
        super();
        this.object = object;
    }

    @Override
    public void run(){
        super.run();
        object.ObjectMethod();
    }
}
