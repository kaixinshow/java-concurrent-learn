package com.zhxin.threadLab.synchronize.chapter2;

/**
 * @ClassName ThreadB2
 * @Description //示例3
 * @Author singleZhang
 * @Email 405780096@qq.com
 * @Date 2020/11/27 0027 上午 10:44
 **/
public class ThreadB2 extends Thread{
    private MyObject2 object;
    public ThreadB2(MyObject2 object){
        super();
        this.object = object;
    }

    @Override
    public void run(){
        super.run();
        object.methodB();
    }
}
