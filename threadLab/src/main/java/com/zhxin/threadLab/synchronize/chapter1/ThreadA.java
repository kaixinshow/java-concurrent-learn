package com.zhxin.threadLab.synchronize.chapter1;

/**
 * @ClassName ThreadA
 * @Description //TODO
 * @Author singleZhang
 * @Email 405780096@qq.com
 * @Date 2020/11/27 0027 上午 8:57
 **/
public class ThreadA extends Thread {
    private PrivateNumber privateNumber;
    public ThreadA(PrivateNumber privateNumber){
        super();
        this.privateNumber = privateNumber;
    }

    @Override
    public void run(){
        super.run();
        privateNumber.changeNumByName("a");
    }
}
