package com.zhxin.threadLab.synchronize.chapter3;

/**
 * @ClassName UserObject
 * @Description // 脏读示例 dirtyRead demo
 * @Author singleZhang
 * @Email 405780096@qq.com
 * @Date 2020/11/27 0027 上午 11:08
 **/
public class UserObject {
    private String userName = "zhangsan";
    private String password = "zhangsan123";

    synchronized public void setValue(String userName,String password){
        try{
            this.userName = userName;
            Thread.sleep(3000);
            this.password = password;
            System.out.println("setValue method,threadName="
                    +Thread.currentThread().getName()
                    +",userName=" +userName+",password="+password);
        }catch (InterruptedException e){
            e.printStackTrace();
        }
    }

    public void getValue(){
        System.out.println("getValue method,threadName="
                +Thread.currentThread().getName()
                +",userName=" +userName+",password="+password);
    }
}
