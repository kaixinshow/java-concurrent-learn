package com.zhxin.threadLab.synchronize.chapter6;

/**
 * @ClassName StringObject
 * @Description // synchronized(String) 使用时注意常量池带来的例外
 * @Author singleZhang
 * @Email 405780096@qq.com
 * @Date 2020/11/27 0027 下午 4:28
 **/
public class StringObject {
    public static void printStr(String s){
        try{
            synchronized (s){
                while(true){
                    System.out.println("ThreadName:"+Thread.currentThread().getName());
                    Thread.sleep(1000);
                }
            }
        }catch(InterruptedException e){
            e.printStackTrace();
        }
    }
}
