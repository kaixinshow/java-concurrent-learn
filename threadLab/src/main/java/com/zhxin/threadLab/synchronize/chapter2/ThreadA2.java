package com.zhxin.threadLab.synchronize.chapter2;

/**
 * @ClassName ThreadA2
 * @Description //示例3
 * @Author singleZhang
 * @Email 405780096@qq.com
 * @Date 2020/11/27 0027 上午 10:43
 **/
public class ThreadA2 extends Thread {
    private MyObject2 object;
    public ThreadA2(MyObject2 object){
        super();
        this.object = object;
    }

    @Override
    public void run(){
        super.run();
        object.methodA();
    }
}
