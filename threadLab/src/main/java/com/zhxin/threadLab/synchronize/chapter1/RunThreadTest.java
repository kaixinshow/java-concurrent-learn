package com.zhxin.threadLab.synchronize.chapter1;

/**
 * @ClassName RunThreadTest
 * @Description //实例变量线程安全/非线程安全测试
 *                // 1. changeNumByName 前不加synchronized
 *                // 2. changeNumByName 前加synchronized
 *                // 3. 多个对象多个锁
 * @Author singleZhang
 * @Email 405780096@qq.com
 * @Date 2020/11/27 0027 上午 9:00
 **/
public class RunThreadTest {

    public static void main(String[] args){

        /* 示例 1,2
        PrivateNumber privateNumber = new PrivateNumber();
        ThreadA a = new ThreadA(privateNumber);
        a.start();
        ThreadB b = new ThreadB(privateNumber);
        b.start();
        */

        /*
        *  3 说明 a,b各自有各自的锁。synchronized 取得的锁是对象锁，这里创建了两个实例对象
        *  2 示例中 是多个线程访问同一个实例对象，哪个线程先执行带synchronized的方法，该线程就获得该方法所属对象的锁Lock，其他线程访问时就需要等待
        *  3 示例中 是多个线程访问多个实例对象，这时JVM会创建多个锁
        * */
        PrivateNumber privateNumberA = new PrivateNumber();
        PrivateNumber privateNumberB = new PrivateNumber();
        ThreadA a = new ThreadA(privateNumberA);
        a.start();
        ThreadB b = new ThreadB(privateNumberB);
        b.start();
    }
}
