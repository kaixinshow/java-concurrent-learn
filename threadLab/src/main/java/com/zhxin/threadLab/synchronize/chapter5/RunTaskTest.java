package com.zhxin.threadLab.synchronize.chapter5;

/**
 * @ClassName RunTaskTest
 * @Description // 异步和同步执行,即不在synchronized块中为异步执行,在synchronized块中则是同步执行 示例测试
 *              // synchronized(this) 代码块 获取的锁 锁定的也是当前对象
 * @Author singleZhang
 * @Email 405780096@qq.com
 * @Date 2020/11/27 0027 下午 3:18
 **/
public class RunTaskTest {

    public static void main(String[] args){
        Task task = new Task();
        ThreadA a = new ThreadA(task);
        a.start();
        ThreadB b = new ThreadB(task);
        b.start();

    }
}
