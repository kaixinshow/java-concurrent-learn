package com.zhxin.threadLab.synchronize.chapter7;

import com.zhxin.threadLab.synchronize.chapter6.ThreadB;

/**
 * @ClassName DealThread
 * @Description // 多线程死锁
 * @Author singleZhang
 * @Email 405780096@qq.com
 * @Date 2020/11/27 0027 下午 4:48
 **/
public class DealThread implements Runnable{
    public String userName;
    public Object obj1 = new Object();
    public Object obj2 = new Object();

    public void setFlag(String userName){
        this.userName = userName;
    }

    @Override
    public void run(){
        if(userName.equals("a")){
            synchronized (obj1){
                try{
                    System.out.println("userName:"+userName);
                    Thread.sleep(3000);
                }catch (InterruptedException e){
                    e.printStackTrace();
                }
            }
            synchronized (obj2){
                System.out.println("按obj1 >> obj2 顺序执行");
            }
        }
        if(userName.equals("b")){
            synchronized (obj2){
                try{
                    System.out.println("userName:"+userName);
                    Thread.sleep(3000);
                }catch (InterruptedException e){
                    e.printStackTrace();
                }
            }
            synchronized (obj1){
                System.out.println("按obj2 >> obj1 顺序执行");
            }
        }
    }

}
