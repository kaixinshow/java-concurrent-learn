package com.zhxin.threadLab.synchronize.chapter3;

/**
 * @ClassName ThreadA
 * @Description // 脏读示例
 * @Author singleZhang
 * @Email 405780096@qq.com
 * @Date 2020/11/27 0027 上午 11:12
 **/
public class ThreadA extends Thread {

    private UserObject object;

    public ThreadA(UserObject object){
        super();
        this.object = object;

    }

    @Override
    public void run(){
        super.run();
        object.setValue("LiSi","LiSi123");
    }

}
