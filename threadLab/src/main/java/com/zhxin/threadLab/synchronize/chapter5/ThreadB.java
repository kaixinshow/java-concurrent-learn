package com.zhxin.threadLab.synchronize.chapter5;

/**
 * @ClassName ThreadB
 * @Description //ThreadB
 * @Author singleZhang
 * @Email 405780096@qq.com
 * @Date 2020/11/27 0027 下午 3:18
 **/
public class ThreadB extends Thread {
    private Task task;
    public ThreadB(Task task){
        super();
        this.task = task;
    }

    @Override
    public void run(){
        super.run();
        task.doLongTimeTask();
    }
}
