package com.zhxin.threadLab.synchronize.chapter6;

/**
 * @ClassName RunObjectTest
 * @Description // synchronized(String) 使用时注意常量池带来的例外
 *              // 示例测试 一直输出A，因为 输入的s相同，线程A一直占据着锁
 *              //结论：synchronized代码块不使用String作为锁对象,使用new Object()
 *              // 修改: object.printStr(new String("AA"));
 * @Author singleZhang
 * @Email 405780096@qq.com
 * @Date 2020/11/27 0027 下午 4:33
 **/
public class RunObjectTest {

    public static void main(String[] args){
        StringObject object = new StringObject();
        ThreadA a = new ThreadA(object);
        a.setName("A");
        a.start();
        ThreadB b = new ThreadB(object);
        b.setName("B");
        b.start();
    }
}
