package com.zhxin.threadLab.synchronize.chapter5;

/**
 * @ClassName Task
 * @Description //异步和同步执行,即不在synchronized块中为异步执行,在synchronized块中则是同步执行
 * @Author singleZhang
 * @Email 405780096@qq.com
 * @Date 2020/11/27 0027 下午 3:12
 **/
public class Task {

    public void doLongTimeTask(){
        for(int i = 0; i < 100; i ++){
            System.out.println("normal! threadName="+Thread.currentThread().getName() + ", i="+i);
        }
        System.out.println("");
        synchronized (this){
            for(int i = 0; i < 100; i++){
                System.out.println("synchronized! threadName="+Thread.currentThread().getName() + ", i="+i);
            }
        }
    }
}
