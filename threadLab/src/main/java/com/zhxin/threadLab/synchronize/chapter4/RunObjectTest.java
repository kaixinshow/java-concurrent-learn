package com.zhxin.threadLab.synchronize.chapter4;

/**
 * @ClassName RunObjectTest
 * @Description // 使用synchronized(this) 同步块 示例测试
 * @Author singleZhang
 * @Email 405780096@qq.com
 * @Date 2020/11/27 0027 上午 11:56
 **/
public class RunObjectTest {

    public static void main(String[] args){
        SynchronizedObject object = new SynchronizedObject();
        ThreadA a = new ThreadA(object);
        a.setName("a");
        a.start();
        ThreadB b = new ThreadB(object);
        b.setName("b");
        b.start();
    }

}
