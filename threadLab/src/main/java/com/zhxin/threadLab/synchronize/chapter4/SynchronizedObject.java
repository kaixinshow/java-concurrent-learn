package com.zhxin.threadLab.synchronize.chapter4;

/**
 * @ClassName SynchronizedObject
 * @Description // 使用synchronized(this) 同步块
 * @Author singleZhang
 * @Email 405780096@qq.com
 * @Date 2020/11/27 0027 上午 11:50
 **/
public class SynchronizedObject {

    public void ObjectMethod(){
        try{
            synchronized (this){

                System.out.println("begin----threadName=" +Thread.currentThread().getName()
                        +", time="+System.currentTimeMillis());
                Thread.sleep(1000);
                System.out.println("end time="+System.currentTimeMillis());
            }
        }catch (InterruptedException e){
            e.printStackTrace();
        }
    }
}
