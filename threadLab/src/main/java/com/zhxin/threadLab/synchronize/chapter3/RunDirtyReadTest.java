package com.zhxin.threadLab.synchronize.chapter3;

/**
 * @ClassName RunDirtyReadTest
 * @Description //脏读示例
 * @Author singleZhang
 * @Email 405780096@qq.com
 * @Date 2020/11/27 0027 上午 11:15
 **/
public class RunDirtyReadTest {

    public static void main(String[] args){
        /*
        * 出现脏读，因为getValue()方法没有synchronized关键字,并不是同步的
        *
        * 解决办法:给getValue()前加上synchronized关键字
        * 说明:
        *    当A线程调用anyObject对象加入synchronized关键字的X方法时，A线程就获得了
        *    X方法锁，更准确地讲，是获得了对象的锁，所以其他线程必须等W程执行完毕才
        *    可以调用X方法，但B线程可以随意调用其他的非synchronized同步方法。
        *    当A线程调用anyObject对象加入synchronized关键字的X方法时，A线程就获得了
        *    X方法所在对象的锁，所以其他线程必须等A线程执行完毕才可以调用X方法，而B
        *    线程如果调用声明了synchronized关键字的非X方法时，必须等A线程将X方法执行
        *    完，也就是释放对象锁后才可以调用。这时A线程已经执行了一个完整的任务，也
        *    就是说username和password这两个实例变量已经同时被赋值，不存在脏读的基本
        *    环境。
         *    脏读一定会出现操作实例变量的情况下，这就是不同线程"争抢"实例变量的结果
        * */
        try{
            UserObject object = new UserObject();
            ThreadA a = new ThreadA(object);
            a.start();
            Thread.sleep(200);
            object.getValue();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

}
