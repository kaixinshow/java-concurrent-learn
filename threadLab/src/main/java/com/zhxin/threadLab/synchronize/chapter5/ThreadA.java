package com.zhxin.threadLab.synchronize.chapter5;

/**
 * @ClassName ThreadA
 * @Description //ThreadA
 * @Author singleZhang
 * @Email 405780096@qq.com
 * @Date 2020/11/27 0027 下午 3:16
 **/
public class ThreadA extends Thread {
    private Task task;
    public ThreadA(Task task){
        super();
        this.task = task;
    }

    @Override
    public void run(){
        super.run();
        task.doLongTimeTask();
    }
}
