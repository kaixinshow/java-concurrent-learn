package com.zhxin.threadLab.synchronize.chapter2;

/**
 * @ClassName MyObject2
 * @Description // 3.改造MyObject
 * @Author singleZhang
 * @Email 405780096@qq.com
 * @Date 2020/11/27 0027 上午 10:36
 **/
public class MyObject2 {

    synchronized public void methodA(){
        try{
            System.out.println("begin-----methodA,current threadName:"
                    +Thread.currentThread().getName());
            Thread.sleep(3000);
            System.out.println("end,endTime="+System.currentTimeMillis());
        }catch (InterruptedException e){
            e.printStackTrace();
        }
    }

     public void methodB(){
        try{
            System.out.println("begin-----methodB,current threadName:"
                    +Thread.currentThread().getName() + ",beginTime="
                    + System.currentTimeMillis());
            Thread.sleep(3000);
            System.out.println("end!");
        }catch (InterruptedException e){
            e.printStackTrace();
        }
    }

}
