package com.zhxin.threadLab.test;

import com.zhxin.threadLab.MyThread;

/**
 * @ClassName RunMyThread
 * @Description // 多线程
 * @Author singleZhang
 * @Email 405780096@qq.com
 * @Date 2020/11/26 0026 上午 10:16
 **/
public class RunMyThread {

    public static void main(String[] args){
        MyThread run = new MyThread();
        Thread a = new Thread(run,"A");
        Thread b = new Thread(run,"B");
        Thread c = new Thread(run,"C");
        Thread d = new Thread(run,"D");
        Thread e = new Thread(run,"E");
        a.start();
        b.start();
        c.start();
        d.start();
        e.start();
    }
}
