package com.zhxin.threadLab.test;

import com.zhxin.threadLab.ThreadPriorityA;
import com.zhxin.threadLab.ThreadPriorityB;

/**
 * @ClassName CountPriorityTest
 * @Description //CountPriorityTest
 * @Author singleZhang
 * @Email 405780096@qq.com
 * @Date 2020/11/26 0026 下午 4:26
 **/
public class CountPriorityTest {

    public static void main(String[] args){
        try {
            ThreadPriorityA a = new ThreadPriorityA();
            a.setPriority(Thread.MIN_PRIORITY);
            a.start();
            ThreadPriorityB b = new ThreadPriorityB();
            b.setPriority(Thread.MAX_PRIORITY);
            b.start();
            Thread.sleep(1000);
            a.stop();// 不建议用stop,不过测试没关系
            b.stop();
            System.out.println("aCount="+a.getCount());
            System.out.println("bCount="+b.getCount());

        }catch (InterruptedException e){
            e.printStackTrace();
        }
    }
}
