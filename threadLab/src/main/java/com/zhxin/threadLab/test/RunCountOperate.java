package com.zhxin.threadLab.test;

import com.zhxin.threadLab.CountOperate;

/**
 * @ClassName RunCountOperate
 * @Description  测试 CountOperate线程类中 构造函数是被线程 main调用的，
 *               run则是被名为Thread-0的线程调用的，run方法是自动调用的方法
 * @Author singleZhang
 * @Email 405780096@qq.com
 * @Date 2020/11/26 0026 下午 2:48
 **/
public class RunCountOperate {

    public static void main(String[] args){
        CountOperate co = new CountOperate();
        /*
         * 此时CountOperate作为参数传入Thread中，其实是CountOperate委托Thread去执行；
         * Thread.currentThread表示当前代码段正在被哪个线程调用的相关信息。
         * this表示的是当前对象，与Thread.currentThread有很大的区别。
         * 参考博客:【多线程】Thread.currentThread()和This的区别
         */
        Thread c = new Thread(co);
        c.setName("co");
        c.start();
    }
}
