package com.zhxin.threadLab.test;

import com.zhxin.threadLab.MyThread;

/**
 * @ClassName CurrentThreadTest
 * @Description // 测试当前线程的名称
 * @Author singleZhang
 * @Email 405780096@qq.com
 * @Date 2020/11/26 0026 上午 11:50
 **/
public class CurrentThreadTest {

    public static void main(String[] args){
        System.out.println(Thread.currentThread().getName());
        MyThread thread = new MyThread();
        thread.start();
    }
}
