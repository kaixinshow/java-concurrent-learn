package com.zhxin.threadLab.singleton.chapter3;

/**
 * @ClassName RunTest
 * @Description //内置静态类实现单例模式 多线程下测试示例
 * @Author singleZhang
 * @Email 405780096@qq.com
 * @Date 2020/11/30 0030 上午 11:55
 **/
public class RunTest {
    public static void main(String[] args) {
        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println(SingletonObject.getMyInstance().hashCode());
            }
        });
        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println(SingletonObject.getMyInstance().hashCode());
            }
        });
        Thread t3 = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println(SingletonObject.getMyInstance().hashCode());
            }
        });
        t1.start();
        t2.start();
        t3.start();
    }
}
