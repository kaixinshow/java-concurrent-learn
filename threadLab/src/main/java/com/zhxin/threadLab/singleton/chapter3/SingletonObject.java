package com.zhxin.threadLab.singleton.chapter3;

/**
 * @ClassName SingletonObject
 * @Description //内置静态类实现单例模式
 * @Author singleZhang
 * @Email 405780096@qq.com
 * @Date 2020/11/30 0030 上午 11:53
 **/
public class SingletonObject {

    private static class SingletonObjectHandler{
        private static SingletonObject singletonObject = new SingletonObject();
    }

    private SingletonObject(){}

    public static SingletonObject getMyInstance(){
        return SingletonObjectHandler.singletonObject;
    }
}
