package com.zhxin.threadLab.singleton.chapter1;

/**
 * @ClassName SingeltonObject
 * @Description //单例模式:饿汉模式 示例
 * @Author singleZhang
 * @Email 405780096@qq.com
 * @Date 2020/11/30 0030 上午 11:02
 **/
public class SingletonObject {

    //立即加载方式,即常说的饿汉模式
    private static SingletonObject myInstance = new SingletonObject();

    private SingletonObject(){ }

    public static SingletonObject getMyInstance(){
        /*
        * 缺点:不能有其他实例变量
        * getMyInstance()方法没同步，可能存在非线程安全问题
        * */
        return myInstance;
    }

}
