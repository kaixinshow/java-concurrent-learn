package com.zhxin.threadLab.singleton.chapter2;

/**
 * @ClassName RunTest
 * @Description //单例模式:延迟加载(懒汉模式) 经典三步骤改进
 * @Author singleZhang
 * @Email 405780096@qq.com
 * @Date 2020/11/30 0030 上午 11:21
 **/
public class RunTest {

    public static void main(String[] args){
        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println(SingletonObject.getMyInstance().hashCode());
            }
        });
        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println(SingletonObject.getMyInstance().hashCode());
            }
        });
        Thread t3 = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println(SingletonObject.getMyInstance().hashCode());
            }
        });
        t1.start();
        t2.start();
        t3.start();

        //示例1 打印出不同的hashCode，说明多线程环境下不安全
        //示例2 加上synchronized关键字后 hashCode一致,实现单例,不过效率低，每个线程都要等待前一个线程释放锁后执行
        // synchronized(this) 代码块 缺点同上
        //示例3 DCL双重检查锁机制,解决单例模式之懒汉模式遇到多线程情况的问题。


    }

}
