package com.zhxin.threadLab.singleton.chapter1;

/**
 * @ClassName RunTest
 * @Description //单例模式:直接加载方式(饿汉模式) 测试示例
 * @Author singleZhang
 * @Email 405780096@qq.com
 * @Date 2020/11/30 0030 上午 11:06
 **/
public class RunTest {


    public static void main(String[] args){
        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println(SingletonObject.getMyInstance().hashCode());
            }
        });
        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println(SingletonObject.getMyInstance().hashCode());
            }
        });
        Thread t3 = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println(SingletonObject.getMyInstance().hashCode());
            }
        });
        t1.start();
        t2.start();
        t3.start();
        //输出的hashcode值为同一个说明实例对象为一个。
    }
}
