package com.zhxin.threadLab.timer.chapter1;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

/**
 * @ClassName TimerTaskRun
 * @Description // TimerTask的使用 示例
 * @Author singleZhang
 * @Email 405780096@qq.com
 * @Date 2020/11/30 0030 上午 10:02
 **/
public class TimerTaskRun {

    static public class TimerTaskA extends TimerTask{

        @Override
        public void run(){
            System.out.println("TaskA 开始了,time:"+new Date());
            //this.cancel();//停止它
        }
    }

    static public class TimerTaskB extends TimerTask{

        @Override
        public void run(){
            System.out.println("TaskB 开始了,time="+ new Date());

        }
    }

    public static void main(String[] args) throws InterruptedException {

        Timer timer = new Timer();
        TimerTaskA taskA = new TimerTaskA();
        TimerTaskB taskB = new TimerTaskB();
        Date now = new Date();
        System.out.println("现在时间是,now="+now);
        timer.schedule(taskA,now);
        timer.schedule(taskB,now);


    }

}
