package com.zhxin.threadLab.concurrenthashmap.chapter1;

/**
 * @ClassName CacheObj
 * @Description //缓存对象类
 * @Author singleZhang
 * @Email 405780096@qq.com
 * @Date 2020/12/24 0024 下午 3:30
 **/
public class CacheObj {
    /**
     * 缓存对象
     */
    private Object CacheValue;
    /**
     * 缓存过期时间
     */
    private Long expireTime;

    CacheObj(Object cacheValue, Long expireTime) {
        CacheValue = cacheValue;
        this.expireTime = expireTime;
    }

    Object getCacheValue() {
        return CacheValue;
    }

    Long getExpireTime() {
        return expireTime;
    }

    @Override
    public String toString() {
        return "CacheObj {" +
                "CacheValue = " + CacheValue +
                ", expireTime = " + expireTime +
                '}';
    }
}
