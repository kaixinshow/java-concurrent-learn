package com.zhxin.threadLab.concurrenthashmap.chapter1;

/**
 * @ClassName CleanCacheThread
 * @Description //清理过期缓存线程
 * @Author singleZhang
 * @Email 405780096@qq.com
 * @Date 2020/12/24 0024 下午 3:37
 **/
public class CleanCacheThread implements Runnable{

    @Override
    public void run() {
        ConcurrentHashMapCacheUtil.setCleanThreadRun();
        while (true) {
            System.out.println("clean thread run ");
            ConcurrentHashMapCacheUtil.deleteTimeOut();
            try {
                Thread.sleep(ConcurrentHashMapCacheUtil.ONE_MINUTE);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
