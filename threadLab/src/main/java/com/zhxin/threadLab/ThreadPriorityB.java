package com.zhxin.threadLab;

/**
 * @ClassName ThreadPriorityB
 * @Description //countPriority
 * @Author singleZhang
 * @Email 405780096@qq.com
 * @Date 2020/11/26 0026 下午 4:26
 **/
public class ThreadPriorityB extends Thread {
    private int count = 0;
    public int getCount(){
        return count;
    }

    @Override
    public void run(){
        while (true){
            count ++;
        }
    }
}
