package com.zhxin.threadLab.threadlocal.chapter1;

/**
 * @ClassName ThreadA
 * @Description //ThreadA
 * @Author singleZhang
 * @Email 405780096@qq.com
 * @Date 2020/11/28 0028 下午 3:37
 **/
public class ThreadA extends Thread {
    @Override
    public void run(){
        try{
            for(int i = 0;i<5;i++){
                Tools.t1.set("ThreadA"+(i+1));
                System.out.println("ThreadA get value :" + Tools.t1.get());
                Thread.sleep(200);
            }
        }catch(InterruptedException e){
            e.printStackTrace();
        }
    }
}
