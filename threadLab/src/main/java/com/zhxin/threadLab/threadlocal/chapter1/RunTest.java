package com.zhxin.threadLab.threadlocal.chapter1;

/**
 * @ClassName RunTest
 * @Description // 验证ThreadLocal变量的隔离性 测试示例
 * @Author singleZhang
 * @Email 405780096@qq.com
 * @Date 2020/11/28 0028 下午 3:41
 **/
public class RunTest {
    public static void main(String[] args){
        try{
            ThreadA a = new ThreadA();
            ThreadB b = new ThreadB();
            a.start();
            b.start();
            for(int i=0;i<5;i++){
                Tools.t1.set("MAIN"+(i+1));
                System.out.println("MAIN get value :" + Tools.t1.get());
                Thread.sleep(200);
            }
        }catch (InterruptedException e){
            e.printStackTrace();
        }
    }
}
