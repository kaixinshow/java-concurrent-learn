package com.zhxin.threadLab.threadlocal.chapter1;

/**
 * @ClassName ThreadB
 * @Description //ThreadB
 * @Author singleZhang
 * @Email 405780096@qq.com
 * @Date 2020/11/28 0028 下午 3:40
 **/
public class ThreadB extends Thread {
    @Override
    public void run(){
        try{
            for(int i = 0;i<5;i++){
                Tools.t1.set("ThreadB"+(i+1));
                System.out.println("ThreadB get value :" + Tools.t1.get());
                Thread.sleep(200);
            }
        }catch (InterruptedException e){
            e.printStackTrace();
        }
    }
}
