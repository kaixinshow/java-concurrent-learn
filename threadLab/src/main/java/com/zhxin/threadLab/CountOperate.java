package com.zhxin.threadLab;

/**
 * @ClassName CountOperate
 * @Description // 【多线程】Thread.currentThread()和This的区别
 * @Author singleZhang
 * @Email 405780096@qq.com
 * @Date 2020/11/26 0026 下午 2:41
 **/
public class CountOperate extends Thread {

    public CountOperate(){
        System.out.println("begin-----CountOperate");
        System.out.println("Thread.currentThread().getName():"+Thread.currentThread().getName());
        System.out.println("this.getName():"+this.getName());
        System.out.println("end-------CountOperate");

    }

    @Override
    public void run(){
        System.out.println("begin-----CountOperate run");
        System.out.println("Thread.currentThread().getName():"+Thread.currentThread().getName());
        System.out.println("this.getName():"+this.getName());
        System.out.println("end-------CountOperate run");
        
    }
}
