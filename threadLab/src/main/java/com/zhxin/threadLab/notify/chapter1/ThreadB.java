package com.zhxin.threadLab.notify.chapter1;

/**
 * @ClassName ThreadB
 * @Description // notify
 * @Author singleZhang
 * @Email 405780096@qq.com
 * @Date 2020/11/28 0028 上午 10:54
 **/
public class ThreadB extends Thread {
    private Object lock;

    public ThreadB(Object object){
        this.lock = object;
    }

    @Override
    public void run(){

        synchronized (lock){
            System.out.println("begin notify! time="+System.currentTimeMillis());
            lock.notify();
            System.out.println("end notify! time="+System.currentTimeMillis());
        }

    }

}
