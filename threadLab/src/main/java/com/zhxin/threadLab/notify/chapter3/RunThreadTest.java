package com.zhxin.threadLab.notify.chapter3;

import com.zhxin.threadLab.notify.chapter3.Consumer;
import com.zhxin.threadLab.notify.chapter3.ThreadC;
import com.zhxin.threadLab.notify.chapter3.ThreadP;

/**
 * @ClassName RunThreadTest
 * @Description //生产者 消费者 假死 示例
 * @Author singleZhang
 * @Email 405780096@qq.com
 * @Date 2020/11/28 0028 下午 2:27
 **/
public class RunThreadTest {


    public static void main(String[] args) throws InterruptedException {
        String lock = new String("");
        Producer p = new Producer(lock);
        Consumer c = new Consumer(lock);
        ThreadP[] pArray = new ThreadP[2];
        ThreadC[] cArray = new ThreadC[2];
        for(int i = 0 ;i<5;i++){
            pArray[i] = new ThreadP(p);
            pArray[i].setName("生产者"+(i+1));
            cArray[i] = new ThreadC(c);
            cArray[i].setName("消费者"+(i+1));
            pArray[i].start();
            cArray[i].start();
        }
        Thread.sleep(10000);
        Thread[] threadArray = new Thread[Thread.currentThread().getThreadGroup().activeCount()];
        Thread.currentThread().getThreadGroup().enumerate(threadArray);
        for(int i = 0; i < threadArray.length;i ++){
            System.out.println("threadName:"+threadArray[i].getName()+" threadState:"+threadArray[i].getState());
        }
    }
}
