package com.zhxin.threadLab.notify.chapter3;

import com.zhxin.threadLab.notify.chapter3.ValueObject;

/**
 * @ClassName Consumer
 * @Description //消费者
 * @Author singleZhang
 * @Email 405780096@qq.com
 * @Date 2020/11/28 0028 下午 2:24
 **/
public class Consumer {
    private String lock;
    public Consumer(String lock){
        this.lock = lock;
    }

    public void getValue(){
        try{
            synchronized (lock){
                if(ValueObject.value.equals("")){
                    System.out.println("消费者,threadName:"+Thread.currentThread().getName() + "waiting!!!");
                    lock.wait();
                }
                System.out.println("消费者,threadName:"+Thread.currentThread().getName() + "runnable!!!");
                ValueObject.value = "";
                lock.notify();
            }
        }catch (InterruptedException e){
            e.printStackTrace();
        }
    }
}
