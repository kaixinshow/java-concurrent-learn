package com.zhxin.threadLab.notify.chapter3;

import com.zhxin.threadLab.notify.chapter3.Producer;

/**
 * @ClassName ThreadP
 * @Description // 生产线程
 * @Author singleZhang
 * @Email 405780096@qq.com
 * @Date 2020/11/28 0028 下午 2:38
 **/
public class ThreadP extends Thread {
    private Producer producer;
    public ThreadP(Producer producer){
        this.producer = producer;
    }

    @Override
    public void run(){
        while(true){
            producer.setValue();
        }
    }
}
