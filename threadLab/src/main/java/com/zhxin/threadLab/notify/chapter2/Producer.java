package com.zhxin.threadLab.notify.chapter2;

/**
 * @ClassName Productor
 * @Description //生产者类
 * @Author singleZhang
 * @Email 405780096@qq.com
 * @Date 2020/11/28 0028 下午 2:14
 **/
public class Producer {
    private String lock;
    public Producer(String lock){
        super();
        this.lock = lock;
    }

    public void setValue(){
       try{
           synchronized (lock){
               if(!ValueObject.value.equals("")){
                   lock.wait();
               }
               String value = "threadNme =" +Thread.currentThread().getName()+"设值,time="+System.currentTimeMillis();
               System.out.println("value值:"+value);
               ValueObject.value = value;
               lock.notifyAll();
           }
       }catch (InterruptedException e){
           e.printStackTrace();
       }
    }
}
