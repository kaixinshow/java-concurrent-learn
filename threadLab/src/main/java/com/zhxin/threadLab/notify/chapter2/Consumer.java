package com.zhxin.threadLab.notify.chapter2;

/**
 * @ClassName Consumer
 * @Description //消费者
 * @Author singleZhang
 * @Email 405780096@qq.com
 * @Date 2020/11/28 0028 下午 2:24
 **/
public class Consumer {
    private String lock;
    public Consumer(String lock){
        this.lock = lock;
    }

    public void getValue(){
        try{
            synchronized (lock){
                if(ValueObject.value.equals("")){
                    lock.wait();
                }
                System.out.println("get的值是:"+ValueObject.value);
                ValueObject.value = "";
                lock.notifyAll();
            }
        }catch (InterruptedException e){
            e.printStackTrace();
        }
    }
}
