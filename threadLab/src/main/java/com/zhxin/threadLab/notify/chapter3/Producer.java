package com.zhxin.threadLab.notify.chapter3;

import com.zhxin.threadLab.notify.chapter3.ValueObject;

/**
 * @ClassName Productor
 * @Description //生产者类
 * @Author singleZhang
 * @Email 405780096@qq.com
 * @Date 2020/11/28 0028 下午 2:14
 **/
public class Producer {
    private String lock;
    public Producer(String lock){
        super();
        this.lock = lock;
    }

    public void setValue(){
       try{
           synchronized (lock){
               if(!ValueObject.value.equals("")){
                   System.out.println("生产者,threadName:"+Thread.currentThread().getName() + "waiting!!!");
                   lock.wait();
               }
               System.out.println("生产者,threadName:"+Thread.currentThread().getName() + "Runnable!!!");
               String value = "time:" + System.currentTimeMillis();
               ValueObject.value = value;
               lock.notify();
           }
       }catch (InterruptedException e){
           e.printStackTrace();
       }
    }
}
