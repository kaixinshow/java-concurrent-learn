package com.zhxin.threadLab.notify.chapter2;

/**
 * @ClassName RunThreadTest
 * @Description //生产者 消费者 示例
 * @Author singleZhang
 * @Email 405780096@qq.com
 * @Date 2020/11/28 0028 下午 2:27
 **/
public class RunThreadTest {

    /*生产者 消费者示例 1
    public static void main(String[] args){
        final String lock = new String("pc");
        Thread t1 = new Thread(new Runnable() {

            @Override
            public void run() {
                try{
                    synchronized (lock){
                        if(!ValueObject.value.equals("")){
                            lock.wait();
                        }
                        String value = "threadNme =" +Thread.currentThread().getName()+"设值,time="+System.currentTimeMillis();
                        System.out.println("value值:"+value);
                        ValueObject.value = value;
                        lock.notifyAll();
                    }
                }catch (InterruptedException e){
                    e.printStackTrace();
                }
            }
        },"t1");
        t1.start();
        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                try{
                    synchronized (lock){
                        if(ValueObject.value.equals("")){
                            lock.wait();
                        }

                        System.out.println("threadName="+Thread.currentThread().getName()+",get的值是:"+ValueObject.value);
                        ValueObject.value = "";
                        lock.notifyAll();
                    }
                }catch (InterruptedException e){
                    e.printStackTrace();
                }
            }
        },"t2");
        t2.start();
    }*/

    //示例2
    public static void main(String[] args){
        String lock = new String("");
        ThreadP p = new ThreadP(new Producer(lock));
        p.start();
        ThreadC c = new ThreadC(new Consumer(lock));
        c.start();
    }
}
