package com.zhxin.threadLab.notify.chapter3;

import com.zhxin.threadLab.notify.chapter3.Consumer;

/**
 * @ClassName ThreadC
 * @Description //消费线程
 * @Author singleZhang
 * @Email 405780096@qq.com
 * @Date 2020/11/28 0028 下午 2:39
 **/
public class ThreadC extends Thread {
    private Consumer consumer;
    public ThreadC(Consumer consumer){
        this.consumer = consumer;
    }

    @Override
    public void run(){
       while(true){
           consumer.getValue();
       }
    }
}
