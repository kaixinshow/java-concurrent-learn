package com.zhxin.threadLab.notify.chapter1;

/**
 * @ClassName ThreadA
 * @Description // wait
 * @Author singleZhang
 * @Email 405780096@qq.com
 * @Date 2020/11/28 0028 上午 10:46
 **/
public class ThreadA extends Thread {
    private Object lock;
    public ThreadA(Object object){
        super();
        this.lock = object;
    }

    @Override
    public void run(){
        try{
            synchronized (lock){
                System.out.println("begin wait! time=" + System.currentTimeMillis());
                lock.wait();
                System.out.println("end wait! time=" + System.currentTimeMillis());
            }
        }catch (InterruptedException e){
            e.printStackTrace();
        }
    }
}
