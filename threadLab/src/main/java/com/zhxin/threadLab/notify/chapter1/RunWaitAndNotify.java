package com.zhxin.threadLab.notify.chapter1;

/**
 * @ClassName RunWaitAndNotify
 * @Description // Thread wait notify Test
 *              // 示例中可以看出，wait需要等notify后边的代码执行完后才得到锁，继续执行
 * @Author singleZhang
 * @Email 405780096@qq.com
 * @Date 2020/11/28 0028 上午 10:56
 **/
public class RunWaitAndNotify {

    public static void main(String[] args){
        try{
            Object lock = new Object(); // 提供同一个锁对象
            ThreadA a = new ThreadA(lock);
            a.start();
            Thread.sleep(1000);
            ThreadB b = new ThreadB(lock);
            b.start();
        } catch (InterruptedException e){
            e.printStackTrace();
        }

    }
}
