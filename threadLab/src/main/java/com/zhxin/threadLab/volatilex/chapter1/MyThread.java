package com.zhxin.threadLab.volatilex.chapter1;

/**
 * @ClassName MyThread
 * @Description //volatile
 * @Author singleZhang
 * @Email 405780096@qq.com
 * @Date 2020/11/28 0028 上午 10:05
 **/
public class MyThread extends Thread {
    volatile public static int count;
    synchronized public static void addCount(){
        for(int i=0;i<100;i++){
            count++;
        }
        System.out.println("count=" + count);
    }

    @Override
    public void run(){
        addCount();
    }
}
