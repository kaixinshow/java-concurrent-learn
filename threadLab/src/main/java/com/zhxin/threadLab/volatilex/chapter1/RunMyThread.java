package com.zhxin.threadLab.volatilex.chapter1;

/**
 * @ClassName RunMyThread
 * @Description //volatile 关键字使用 示例
 * @Author singleZhang
 * @Email 405780096@qq.com
 * @Date 2020/11/28 0028 上午 10:07
 **/
public class RunMyThread {

    public static void main(String[] args){
        MyThread[] tArr = new MyThread[100];
        for(int i=0;i<100;i++){
            tArr[i] = new MyThread();
            tArr[i].start();
        }

    }
}
