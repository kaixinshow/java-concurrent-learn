package com.zhxin.threadLab.volatilex.chapter2;

/**
 * @ClassName RunTest
 * @Description // 原子性操作 AtomicInteger 示例
 * @Author singleZhang
 * @Email 405780096@qq.com
 * @Date 2020/12/1 0001 上午 8:47
 **/
public class RunTest {
    public static void main(String[] args){
        AddCountThread countService = new AddCountThread();
        Thread t1 = new Thread(countService);
        t1.start();
        Thread t2 = new Thread(countService);
        t2.start();
//        AddCountThread t1 = new AddCountThread();
//        AddCountThread t2 = new AddCountThread();
//        t1.start();
//        t2.start();
    }
}
