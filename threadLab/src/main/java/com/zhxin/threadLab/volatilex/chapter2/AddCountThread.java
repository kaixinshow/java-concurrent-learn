package com.zhxin.threadLab.volatilex.chapter2;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @ClassName AddCountThread
 * @Description // 原子性操作 AtomicInteger 示例
 * @Author singleZhang
 * @Email 405780096@qq.com
 * @Date 2020/12/1 0001 上午 8:43
 **/
public class AddCountThread extends Thread{

    volatile private AtomicInteger count = new AtomicInteger(0);

    @Override
    public void run(){
        for(int i = 0;i < 5; i++){
           System.out.println(count.incrementAndGet());
        }
    }


}
